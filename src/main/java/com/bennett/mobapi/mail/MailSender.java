package com.bennett.mobapi.mail;

import java.net.HttpURLConnection;
import java.net.URL;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component("javasampleapproachMailSender")
@PropertySource("classpath:application.properties")
public class MailSender {

    @Value("${spring.sms.url}")
    private String urlpath;
    
    @Autowired
    JavaMailSender javaMailSender;

   private static Logger logger = LoggerFactory.getLogger(MailSender.class);

    public boolean sendMail(String from, String to, String subject, String body) {
        try {
            InternetAddress[] iAdressArray = InternetAddress.parse(to);
            MimeMessage mail = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail, false, "utf-8");
            helper.setTo(iAdressArray);
            helper.setSubject(subject);
            helper.setFrom(from);
           // helper.setBcc(from);
            mail.setContent(body, "text/html");
            javaMailSender.send(mail);
             logger.info("MailSender :: sendMail to = " + to + ",  subject " + subject);
            return true;
        } catch (Exception e) {
            logger.error("MailSender :: sendMail to = " + to + ",  subject " + subject, e);
            return false;
        }

    }

    public String sendSms(String requestUrl) {
        String status = "";
        try {
            URL url = new URL(urlpath+""+requestUrl);
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            status = uc.getResponseMessage();
            uc.disconnect();
             logger.info("MailSender :: sendSms to = " + requestUrl);
        } catch (Exception e) {
            logger.error("Problem to sendSms :: " + requestUrl + " :: ", e);
        }
        return status;
    }
}

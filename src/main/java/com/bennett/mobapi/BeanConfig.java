/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bennett.mobapi;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Yatendra Singh
 */
@Configuration
@ComponentScan
public class BeanConfig {

    public class AppConfig {

        @Bean
        public DataSource dataSource() {
            DriverManagerDataSource ds = new DriverManagerDataSource();
            ds.setDriverClassName(oracle.jdbc.driver.OracleDriver.class.getName());
            ds.setUrl("jdbc:oracle:thin:@10.6.0.166:1521/orcl");
            ds.setUsername("BENNETTCAMPUS");
            ds.setPassword("godtusigreatho");
            
//            ds.setUsername("BUCAMPUS31052018");
//            ds.setPassword("bucampus31052018");
//            ds.setUsername("bu01082018");
//            ds.setPassword("bu2018");
            return ds;
        }

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bennett.mobapi.service;

import com.bennett.mobapi.dao.MobileOracelDao;
import com.bennett.mobapi.util.EncryptDecryptString;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.xml.bind.DatatypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

/**
 *
 * @author acer
 */
@Service
public class PApprovalService {

    static final Logger logger = LoggerFactory.getLogger(MobileOracelService.class);
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    @Autowired
    MobileOracelDao oracelDao;

    @Autowired
    EncryptDecryptString eds;

    public String getParentStatus(String stuid) {
        StringBuilder sb = new StringBuilder(stuid);
        Map mp = null;
        try {
            sb.reverse();
            String decoded = new String(DatatypeConverter.parseBase64Binary(sb.toString()));
            mp = (LinkedCaseInsensitiveMap) oracelDao.getParentStatus(decoded).get(0);
        } catch (Exception e) {

        }
        return htmlPage(stuid, mp.get("parentaremarks").toString(), mp.get("apstuts").toString());
    }

    public int getHOParentApproval(Map hostelstu) {
        StringBuilder sb = new StringBuilder(hostelstu.get("id").toString());
        hostelstu.put("requestdatetime", sdfDate.format(new Date()));
        hostelstu.put("requestid", new String(DatatypeConverter.parseBase64Binary(sb.reverse().toString())));

        return oracelDao.getHOParentApproval(hostelstu);
    }

    private String htmlPage(String id, String remark, String status) {
        StringBuilder sb = new StringBuilder();

        sb.append("<!DOCTYPE html> ");
        sb.append("<html lang='en'> ");
        sb.append("    <head> ");
        sb.append("        <title>Bennett University</title> ");
        sb.append("        <meta charset='utf-8'> ");
        sb.append("        <link rel=\"shortcut icon\" href=\"https://www.bennett.edu.in/wp-content/themes/twentysixteen/images/favicon.ico\" type=\"image/vnd.microsoft.icon\" />");
        sb.append("        <meta name='viewport' content='width=device-width, initial-scale=1'> ");
        sb.append("        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'> ");
        sb.append("        <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script> ");
        sb.append("        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script> ");
        sb.append("        <script> ");
        sb.append("            $(document).ready(function () { ");
        if (status.equals("A") || status.equals("C")) {
            sb.append("                $('#myModal').modal({ ");
        } else {
            sb.append("                $('#myfromModal').modal({ ");
        }
        sb.append("                    backdrop: 'static', ");
        sb.append("                    keyboard: false ");
        sb.append("                }); ");
        sb.append("            }); ");
        sb.append("            function sendApprovalRequest(status) { ");
        sb.append("                var pr = {}; ");
        sb.append("                pr.status = status; ");
        sb.append("                pr.remark = $('#comment').val(); ");
        sb.append("                pr.id = '").append(id).append("'; ");
        sb.append("                $.ajax({ ");
        sb.append("                    url: '/v2/parentapv', ");
        sb.append("                    type: 'post', ");
        sb.append("                    contentType: 'application/json', ");
        sb.append("                    success: function (data) { ");
        sb.append("                        location.reload(); ");
        sb.append("                        $('#myfromModal').hide(); ");
        sb.append("                        $('#myModal').modal({ ");
        sb.append("                            backdrop: 'static', ");
        sb.append("                            keyboard: false ");
        sb.append("                        }); ");
        sb.append("                    }, ");
        sb.append("                    data: JSON.stringify(pr) ");
        sb.append("                }); ");
        sb.append("            } ");
        sb.append("        </script> ");
        sb.append("    </head> ");
        sb.append("    <body> ");
        sb.append("        <div class='container'> ");
        sb.append(" ");
        sb.append("            <div class='modal fade' id='myfromModal' role='dialog'> ");
        sb.append("                <div class='modal-dialog modal-lg'> ");
        sb.append("                    <div class='modal-content'> ");
        sb.append("                        <div class='modal-header'> ");
        sb.append("                            <h4 class='modal-title'>Hostel Outgoing Approval</h4> ");
        sb.append("                        </div> ");
        sb.append("                        <div class='modal-body'> ");
        sb.append("                            <table style='width: 80%;margin-left: 10%'> ");
        sb.append("                                <tr> ");
        sb.append("                                    <td></td> ");
        sb.append("                                    <td> ");
        sb.append("                                        <label for='comment'>Remark</label> ");
        sb.append("                                        <textarea class='form-control' rows='5' id='comment'></textarea> ");
        sb.append("                                    </td> ");
        sb.append("                                    <td></td> ");
        sb.append("                                </tr> ");
        sb.append("                                <tr> ");
        sb.append("                                    <td></td> ");
        sb.append("                                    <td> <button type='submit' class='btn btn-primary' onclick='sendApprovalRequest(\"A\")'>Approve</button>  ");
        sb.append("                                        <button type='submit' class='btn btn-primary' onclick='sendApprovalRequest(\"C\")'>Cancel</button></td> ");
        sb.append("                                    <td></td> ");
        sb.append(" ");
        sb.append("                                </tr> ");
        sb.append("                            </table> ");
        sb.append("                        </div> ");
        sb.append("                        <div class='modal-footer'> ");
        sb.append(" ");
        sb.append("                        </div> ");
        sb.append("                    </div> ");
        sb.append("                </div> ");
        sb.append("            </div> ");
        sb.append("            <div class='modal fade' id='myModal' role='dialog'> ");
        sb.append("                <div class='modal-dialog modal-sm'> ");
        sb.append("                    <div class='modal-content'> ");
        sb.append("                        <div class='modal-header'> ");
        sb.append("                            <h4 class='modal-title'>Approval Status</h4> ");
        sb.append("                        </div> ");
        sb.append("                        <div class='modal-body'> ");
        sb.append("                            <p>" + (status.equals("A") ? "<span class='glyphicon glyphicon-ok-sign' style='font-size: x-large;color: green'>" : "<span class='glyphicon glyphicon-remove-sign' style='font-size: x-large;color: red'>") + "</span> <span style='vertical-align: top'>").append(remark).append("</span></p> ");
        sb.append("                        </div> ");
        sb.append("                        <div class='modal-footer'> ");
        sb.append("                             ");
        sb.append("                        </div> ");
        sb.append("                    </div> ");
        sb.append("                </div> ");
        sb.append("            </div> ");
        sb.append("        </div> ");
        sb.append("    </body> ");
        sb.append("</html>");

        return sb.toString();
    }

    public static void main(String[] args) {
        EncryptDecryptString eds = new EncryptDecryptString();
        System.out.println(eds.encrypt("BENNHRQT1809A0000126"));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bennett.mobapi.service;

import com.bennett.mobapi.dao.MobileOracelDao;
import com.bennett.mobapi.mail.MailSender;
import com.bennett.mobapi.util.EncryptDecryptString;
import com.bennett.mobapi.util.LdapActiveDirectory;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import javax.xml.bind.DatatypeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

/**
 *
 * @author Yatendra Singh
 */
@Service
public class MobileOracelService {

    static final Logger logger = LoggerFactory.getLogger(MobileOracelService.class);
    private static DecimalFormat df = new DecimalFormat(".##");
    static Map sessionmap = new HashMap();
    private List errorli = new ArrayList();
    private static HttpURLConnection con;
//    private static String url = "http://10.6.0.129/appfiles/login.php";
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Autowired
    @Qualifier("javasampleapproachMailSender")
    public MailSender mailSender;

    @Autowired
    MobileOracelDao oracelDao;

    @Autowired
    EncryptDecryptString eds;
    @Autowired
    LdapActiveDirectory lad;

    public List getAttendanceDetail(String refreshtocken) {
        List attendancelist = new ArrayList();
        Set subcodelist = new HashSet();
        if (sessionmap.containsKey(refreshtocken)) {
            List l = oracelDao.getAttendanceDetail((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken));
            for (int i = 0; i < l.size(); i++) {
                Map curr = ((LinkedCaseInsensitiveMap) l.get(i));
                subcodelist.add(curr.get("SUBJECTCODE").toString());
            }
            List<String> deDupStringList = new ArrayList<>(new HashSet<>(subcodelist));
            Map attmap = new HashMap();
            attmap.put("L", 0);
            attmap.put("P", 0);
            attmap.put("T", 0);
            attmap.put("lcomponentid", 0);
            attmap.put("pcomponentid", 0);
            attmap.put("tcomponentid", 0);
            attmap.put("total", 0);
            for (int j = 0; j < deDupStringList.size(); j++) {
            double tot = 0.0, totp = 0.0;
                for (int i = 0; i < l.size(); i++) {
                    Map curr = ((LinkedCaseInsensitiveMap) l.get(i));
                    if (deDupStringList.get(j).equals(curr.get("SUBJECTCODE").toString())) {
                        attmap.put("subjectcode", curr.get("SUBJECTCODE").toString());

                        if ("L".equalsIgnoreCase(curr.get("SUBJECTCOMPONENTCODE").toString())) {
                            attmap.put("L", (int) Math.round(Double.parseDouble(curr.get("PRESENT").toString())));
                            attmap.put("lcomponentid", curr.get("SUBJECTID").toString() + "@@" + curr.get("SUBJECTCOMPONENTID").toString());
                        } else if ("P".equalsIgnoreCase(curr.get("SUBJECTCOMPONENTCODE").toString())) {
                            attmap.put("P", (int) Math.round(Double.parseDouble(curr.get("PRESENT").toString())));
                            attmap.put("pcomponentid", curr.get("SUBJECTID").toString() + "@@" + curr.get("SUBJECTCOMPONENTID").toString());
                        } else {
                            attmap.put("T", (int) Math.round(Double.parseDouble(curr.get("PRESENT").toString())));
                            attmap.put("tcomponentid", curr.get("SUBJECTID").toString() + "@@" + curr.get("SUBJECTCOMPONENTID").toString());
                        }
                        tot = tot + Double.parseDouble(curr.get("total").toString());
                        totp = totp + Double.parseDouble(curr.get("totalpres").toString());

                    }
                }
                attmap.put("lpt", (int) Math.round((totp / tot) * 100) + "");
                attendancelist.add(attmap);
                attmap = new HashMap();
                attmap.put("L", 0);
                attmap.put("P", 0);
                attmap.put("T", 0);
                attmap.put("lcomponentid", 0);
                attmap.put("pcomponentid", 0);
                attmap.put("tcomponentid", 0);
                attmap.put("total", 0);
                attmap.put("lpt", 0);
            }

            return attendancelist;
        } else {
            return errorli;
        }
    }

    public String getRefreshToken(Map accesstoken) {
        ObjectMapper mapper = new ObjectMapper();
        String refreshtocken = "";
        try {
            Map stdata = null;
            String decoded = new String(eds.decrypt(accesstoken.get("astoken").toString()));
            logger.info("decode====" + decoded);
            Map studetail = new HashMap();

            studetail = mapper.readValue(decoded, HashMap.class);
            if ("P".equalsIgnoreCase(studetail.get("stutype").toString()) || "S".equalsIgnoreCase(studetail.get("stutype").toString())) {
                stdata = (LinkedCaseInsensitiveMap) (oracelDao.getRefreshToken(studetail).get(0));
            } else if ("E".equalsIgnoreCase(studetail.get("stutype").toString())) {
                stdata = (LinkedCaseInsensitiveMap) (oracelDao.getRefreshTokenForEmployee(studetail).get(0));

            }
            if (stdata != null && !stdata.isEmpty()) {
                stdata.put("utype", studetail.get("stutype").toString());
                if ("P".equalsIgnoreCase(studetail.get("stutype").toString()) || "S".equalsIgnoreCase(studetail.get("stutype").toString())) {
                    refreshtocken = (UUID.randomUUID().toString().replace("-", "")) + (studetail.get("studentid").toString().replace("BUINT", ""));
                } else if ("E".equalsIgnoreCase(studetail.get("stutype").toString())) {
                    refreshtocken = (UUID.randomUUID().toString().replace("-", "")) + (studetail.get("studentid").toString().replace("BUINT", ""));
                }
                if (sessionmap.containsKey(accesstoken.get("refreshtoken").toString())) {
                    sessionmap.remove(accesstoken.get("refreshtoken").toString());
                }
                sessionmap.put(refreshtocken, stdata);
                logger.info("getRefreshToken == " + sessionmap.size());
            } else {
                refreshtocken = "";
            }
        } catch (Exception e) {
            logger.error("Problem to getRefreshToken :: ");
            refreshtocken = "";
        }
        return refreshtocken;
    }

    public boolean getRefreshTokenDestroy(String refreshtocken) {
        sessionmap.remove(refreshtocken);
        boolean b = !sessionmap.containsKey(refreshtocken);
        return b;
    }

    public byte[] getUserProfile(String refreshtocken) {
        try {
            return oracelDao.getUserProfilePic((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken));
        } catch (Exception e) {
            logger.error("Problem to getUserProfilePic :: ");
        }
        return null;
    }

    public List getMobileMenu(String refreshtocken) {
        try {
            if (sessionmap.containsKey(refreshtocken)) {
                return oracelDao.getMobileMenu();
            } else {
                return errorli;
            }

        } catch (Exception e) {
            logger.error("Problem to getMobileMenu :: ");
            return errorli;
        }
    }

    public List getFoodMenu(String refreshtocken) {
        try {
            if (sessionmap.containsKey(refreshtocken)) {
                List l = oracelDao.getFoodMenu();
                return l;
            } else {
                return errorli;
            }

        } catch (Exception e) {
            logger.error("Problem to getFoodMenu :: ");
            return errorli;
        }
    }

    public List getAttendanceSubDetail(Map accesstoken) {
        List subdetail = null;
        if (sessionmap.containsKey(accesstoken.get("refreshtoken").toString())) {
            LinkedCaseInsensitiveMap attsub = (LinkedCaseInsensitiveMap) sessionmap.get(accesstoken.get("refreshtoken").toString());
            attsub.put("subid", accesstoken.get("subid"));
            subdetail = oracelDao.getAttendanceSubDetail(attsub);
            for (int i = 0; i < subdetail.size(); i++) {
                if (((LinkedCaseInsensitiveMap) subdetail.get(i)).get("attper").toString().equalsIgnoreCase("Y")) {
                    ((LinkedCaseInsensitiveMap) subdetail.get(i)).put("attper", "Present");
                } else {
                    ((LinkedCaseInsensitiveMap) subdetail.get(i)).put("attper", "Absent");

                }
            }
        }

        return subdetail;

    }

    public List getStynumberResult(String refreshtocken) {
        List sysresult = new ArrayList();
        if (sessionmap.containsKey(refreshtocken)) {
            sysresult = oracelDao.getStynumberResult((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken));
        }
        return sysresult;
    }

    public List getStynumberByResult(Map accesstoken) {
        List subdetail = null;
        if (sessionmap.containsKey(accesstoken.get("refreshtoken").toString())) {
            LinkedCaseInsensitiveMap attsub = (LinkedCaseInsensitiveMap) sessionmap.get(accesstoken.get("refreshtoken").toString());
            attsub.put("sysnum", accesstoken.get("sysnum"));
            subdetail = oracelDao.getStynumberByResult(attsub);
            for (int i = 0; i < subdetail.size(); i++) {
                if (((LinkedCaseInsensitiveMap) subdetail.get(i)).get("FAIL").toString().equalsIgnoreCase("Y")) {
                    ((LinkedCaseInsensitiveMap) subdetail.get(i)).put("FAIL", "Fail");
                } else {
                    ((LinkedCaseInsensitiveMap) subdetail.get(i)).put("FAIL", "Pass");

                }
            }
        }

        return subdetail;
    }

    public List getTimeTable(String refreshtocken) {
        List sysresult = new ArrayList();
        if (sessionmap.containsKey(refreshtocken)) {
            sysresult = oracelDao.getTimeTable((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken));
        }
        return sysresult;
    }

    public List getEmergencyService(String refreshtocken) {
        List sysresult = new ArrayList();
        if (sessionmap.containsKey(refreshtocken)) {
            sysresult = oracelDao.getEmergencyService((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken));
        }
        return sysresult;
    }

    public List getEmergencyAlertService(Map accesstoken) {
        List sysalert = new ArrayList();
        try {
            if (sessionmap.containsKey(accesstoken.get("refreshtoken").toString())) {
                sysalert = oracelDao.getEmergencyStudentdetail((LinkedCaseInsensitiveMap) sessionmap.get(accesstoken.get("refreshtoken").toString()));
                LinkedCaseInsensitiveMap stu = (LinkedCaseInsensitiveMap) sysalert.get(0);

                StringBuilder stubasicdata = new StringBuilder();
                stubasicdata.append("<b>Enrollment no:</b> ").append(stu.get("ENROLLMENTNO")).append(", <br/>");
                stubasicdata.append("<b>Student Name:</b> ").append(stu.get("programcode")).append(", <br/>");
                stubasicdata.append("<b>Program:</b> ").append(stu.get("programcode")).append(", <br/>");
                stubasicdata.append("<b>Mobile No:</b> ").append(stu.get("SCELLNO")).append(", <br/>");
                stubasicdata.append("<b>Email ID:</b> ").append(stu.get("STUDENTPERSONALEMAILID")).append(", <br/>");
                stubasicdata.append("<b>Hostel Code:</b> ").append(stu.get("hostelcode")).append(", ").append(stu.get("roomcode")).append(" <br/>");

                StringBuilder smsdata = new StringBuilder();
                smsdata.append("Enrollment no: ").append(stu.get("ENROLLMENTNO")).append(",");
                smsdata.append("Student Name:").append(stu.get("programcode")).append(", ");
                smsdata.append("Program:").append(stu.get("programcode")).append(",");
                smsdata.append("Mobile No:").append(stu.get("SCELLNO")).append(",");
                smsdata.append("Email ID:").append(stu.get("STUDENTPERSONALEMAILID")).append(",");
                smsdata.append("Hostel Code:").append(stu.get("hostelcode")).append(", ").append(stu.get("roomcode")).append(" ");

                List deplist = oracelDao.getDepartmentEmergencyService(accesstoken);
                if (deplist.size() > 0) {
                    LinkedCaseInsensitiveMap dep = (LinkedCaseInsensitiveMap) deplist.get(0);
                    StringBuilder smsurl = new StringBuilder();
                    smsurl.append("&dest_mobileno=").append(dep.get("contectno"));
                    smsurl.append("&message=").append(URLEncoder.encode(dep.get("SMSTEXT").toString() + smsdata.toString(), "UTF-8"));
                    smsurl.append("&response=Y");

                    String from = "no-reply@bennett.edu.in";
                    String to = dep.get("emailid").toString();
                    String subject = dep.get("EMAILSUBJECT").toString();
                    String body = "<b>" + dep.get("MAILTEXT").toString() + "</b><br/><br/>" + stubasicdata;
                    sysalert = new ArrayList();
                    mailSender.sendSms(smsurl.toString());
                    if (mailSender.sendMail(from, to, subject, body)) {
                        sysalert.add("SMS And Email send done");
                    } else {
                        sysalert.add("Email send fail");

                    }
                }
            }
        } catch (Exception e) {
            sysalert.add("MS And Email send fail.");
            return sysalert;
        }
        return sysalert;
    }

    public List getStudentProfileData(String refreshtocken) {
        List sysresult = new ArrayList();
        if (sessionmap.containsKey(refreshtocken)) {
            sysresult = oracelDao.getEmergencyStudentdetail((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken));
        }
        return sysresult;
    }

    public List getMobileMenuOrder(Map refreshtocken) {
//        System.out.println(">>>>>>sessionmap==="+sessionmap);
//        System.out.println("<<<>>>>=="+refreshtocken);
        List mobmenu = new ArrayList();
        if (sessionmap.containsKey(refreshtocken.get("refreshtoken").toString())) {
            mobmenu = oracelDao.getMobileMenuOrder(refreshtocken.get("utype").toString());
            for (int i = 0; i < mobmenu.size(); i++) {
                ((LinkedCaseInsensitiveMap) mobmenu.get(i)).put("ICONNAME", (((LinkedCaseInsensitiveMap) mobmenu.get(i)).get("ICONLABEL").toString().toLowerCase()).replace(" ", "") + "logo");
                ((LinkedCaseInsensitiveMap) mobmenu.get(i)).put("ICONLABEL", ((LinkedCaseInsensitiveMap) mobmenu.get(i)).get("ICONLABEL"));
                ((LinkedCaseInsensitiveMap) mobmenu.get(i)).put("URL", ((LinkedCaseInsensitiveMap) mobmenu.get(i)).get("URL"));
            }
        } else {
            mobmenu = oracelDao.getMobileMenuOrder("G");
            for (int i = 0; i < mobmenu.size(); i++) {
                ((LinkedCaseInsensitiveMap) mobmenu.get(i)).put("ICONNAME", (((LinkedCaseInsensitiveMap) mobmenu.get(i)).get("ICONLABEL").toString().toLowerCase()).replace(" ", "") + "logo");
                ((LinkedCaseInsensitiveMap) mobmenu.get(i)).put("ICONLABEL", ((LinkedCaseInsensitiveMap) mobmenu.get(i)).get("ICONLABEL"));
                ((LinkedCaseInsensitiveMap) mobmenu.get(i)).put("URL", ((LinkedCaseInsensitiveMap) mobmenu.get(i)).get("URL"));
            }
        }

        //System.out.println(">>>>>>>>"+mobmenu);
        return mobmenu;
    }

    public Map getLoginDetail(String loginstr) {
        ObjectMapper mapper = new ObjectMapper();
        StringBuilder sb = new StringBuilder(loginstr);
        Map stdata = new HashMap();
        try {
            sb.reverse();
            String decoded = new String(DatatypeConverter.parseBase64Binary(sb.toString()));
            Map studetail = mapper.readValue(decoded, HashMap.class);
            studetail.put("username", studetail.get("username").toString().split("@")[0]);
            Map member = ((LinkedCaseInsensitiveMap) oracelDao.getMemberDetail(studetail.get("username").toString()).get(0));
            if (member.size() > 0) {
                Map ldapstr = getLoginResponse(studetail.get("username").toString(), studetail.get("password").toString(), member.get("membertype").toString());
                if (!ldapstr.isEmpty()) {
                    String astoken = "{\"studentid\":\"" + member.get("memberid") + "\",\"stutype\":\"" + member.get("membertype") + "\"}";
                    stdata.put("astoken", eds.encrypt(astoken) + "");
                    stdata.put("stutype", member.get("membertype"));
                    stdata.put("refreshtoken", "abcd");
                    String stulogin = getRefreshToken(stdata);
                    if (!"".equals(stulogin)) {
                        stdata.put("refreshtoken", stulogin);
                        if ("S".equalsIgnoreCase(stdata.get("stutype").toString())) {
                            stdata.put("uname", ((LinkedCaseInsensitiveMap) (oracelDao.getEmergencyStudentdetail((LinkedCaseInsensitiveMap) sessionmap.get(stulogin))).get(0)).get("name"));
                        } else if ("E".equalsIgnoreCase(stdata.get("stutype").toString())) {
                            stdata.put("uname", ((LinkedCaseInsensitiveMap) (oracelDao.getEmergencyEmployeeDetail((LinkedCaseInsensitiveMap) sessionmap.get(stulogin))).get(0)).get("employeename"));
                        } else if ("P".equalsIgnoreCase(stdata.get("stutype").toString())) {
                            stdata.put("uname", ((LinkedCaseInsensitiveMap) (oracelDao.getEmergencyStudentdetail((LinkedCaseInsensitiveMap) sessionmap.get(stulogin))).get(0)).get("name"));
                        }
                    } else {
                        stdata = new HashMap();
                    }
                }
            }
        } catch (Exception e) {
            stdata = new HashMap();
            logger.error("Problem to getLoginDetail service :: " + loginstr);
        }
        return stdata;
    }

    private Map getLoginResponse(String username, String password, String utype) {
        Map map = new HashMap();
        map.put("userid", username);
        map.put("password", password);
        map.put("utype", utype);
        Map userdetail = lad.getLDAPValidate(map);
        if (!userdetail.isEmpty()) {
            return userdetail;
        } else {
            return new HashMap();
        }
    }

    public Map getHostelOutPurpose(String refreshtocken) {
        Map hopur = new HashMap();
        StringBuilder sb = new StringBuilder();
        if (sessionmap.containsKey(refreshtocken)) {
            try {
                Map hostel = (LinkedCaseInsensitiveMap) (oracelDao.getStudentHostelDetail(((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken)).get("studentid").toString()).get(0));
                if (hostel.size() > 0) {
                    sb.append("");
                    List purposelist = oracelDao.getHostelOutPurpose();
                    for (int i = 0; i < purposelist.size(); i++) {
                        if ((purposelist.size() - 1) != i) {
                            sb.append("").append(((LinkedCaseInsensitiveMap) purposelist.get(i)).get("HOSTELOUTPURPOSE")).append("$");
                        } else {
                            sb.append("").append(((LinkedCaseInsensitiveMap) purposelist.get(i)).get("HOSTELOUTPURPOSE")).append("");
                        }
                    }
                    sb.append("");
                    hopur.put("after", "00:00");
                    hopur.put("before", "20:00");
                    hopur.put("purpose", sb);
                }
            } catch (Exception e) {
                return new HashMap();
            }
        }
        return hopur;
    }

    public int getHostelOutStuDetail(Map hostreqdetail) {
        try {
            if (sessionmap.containsKey(hostreqdetail.get("refreshtoken").toString())) {
                Map mp = (LinkedCaseInsensitiveMap) sessionmap.get(hostreqdetail.get("refreshtoken").toString());
                if (Integer.parseInt(oracelDao.getReqCount(mp.get("studentid").toString(),hostreqdetail.get("checkoutdatetime").toString())) == 0) {
                    Map hostel = (LinkedCaseInsensitiveMap) (oracelDao.getStudentHostelDetail(mp.get("studentid").toString()).get(0));
                    hostreqdetail.put("studentid", mp.get("studentid"));
                    hostreqdetail.put("requestid", "BMGTHRQT1712A" + (int) ((Math.random() * 9000) + 1000));
                    hostreqdetail.put("requestno", "HLNO2017122600");
                    hostreqdetail.put("hostelid", hostel.get("hostelid"));
                    hostreqdetail.put("roomid", hostel.get("roomid"));
                    hostreqdetail.put("status", "P");
                    hostreqdetail.put("requestbymemberid", mp.get("studentid"));
                    hostreqdetail.put("requestdatetime", sdfDate.format(new Date()));
                    hostreqdetail.put("requestbymembertype", "S");
                    if (oracelDao.getHostelOutStuDetail(hostreqdetail) == 1) {
                        List conlist = oracelDao.getParentDetail(mp.get("studentid").toString());
                        if (conlist.size() > 0) {
                            LinkedCaseInsensitiveMap dep = (LinkedCaseInsensitiveMap) conlist.get(0);
                            String from = "no-reply@bennett.edu.in";
                            String to = dep.get("pemailid").toString();
                           // String to = "yatendra.singh@bennett.edu.in";
                            String subject = "For hostel out pass approval";
                            StringBuilder body = new StringBuilder();
                            body.append("<b>Dear Sir/Madam</b><br><br>");
                            body.append("This is inform you that your ward has decided to go out from <b>" + (hostreqdetail.get("checkoutdatetime")) + "</b> to <b>" + (hostreqdetail.get("expactedcheckindatetime")) + "</b>.<br> ");
                            body.append("Kindly approve the same though App.<br><br>");
                            body.append("Thanks & Regards<br>Hostel Admin");
                            StringBuilder smsurl = new StringBuilder();
                           // smsurl.append("&dest_mobileno=").append("9540898041");
                            smsurl.append("&dest_mobileno=").append(dep.get("pcellno"));
                            smsurl.append("&message=").append(URLEncoder.encode("This is inform you that your ward has decided to go out from  " + (hostreqdetail.get("checkoutdatetime")) + " to  " + (hostreqdetail.get("expactedcheckindatetime")) + " kindly approve the same though App", "UTF-8"));
                            smsurl.append("&response=Y");
                            mailSender.sendSms(smsurl.toString());
                            mailSender.sendMail(from, to, subject, body.toString());
                        }
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (Exception e) {
            logger.error("MobileOracelService : problem in getHostelOutStuDetail :: " + hostreqdetail);
            return 0;
        }
    }

    public List getStudentRequestStatus(String refreshtocken) {
        if (sessionmap.containsKey(refreshtocken)) {
            if ("E".equals(((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken)).get("utype").toString())) {
                List li = oracelDao.getWardenId(((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken)).get("employeeid").toString());
                if (li.size() > 0 && !li.isEmpty()) {
                    return oracelDao.getWardenRequestStatus();
                }
            } else {
                return oracelDao.getStudentRequestStatus(((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken)).get("studentid").toString());
            }
        }
        return new ArrayList();
    }

    public String getNewOtp(String refreshtocken) {
        String myotp = "0000";
        if (sessionmap.containsKey(refreshtocken)) {
            List conlist = oracelDao.getParentDetail(((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken)).get("studentid").toString());
            if (conlist.size() > 0) {
                myotp = (((new Random()).nextInt(8999)) + 1000) + "";
                try {
                    LinkedCaseInsensitiveMap dep = (LinkedCaseInsensitiveMap) conlist.get(0);
                    StringBuilder smsurl = new StringBuilder();
                    smsurl.append("&dest_mobileno=").append(dep.get("pcellno"));
                    //smsurl.append("&dest_mobileno=").append("9540898041");
                    smsurl.append("&message=").append(URLEncoder.encode("To Login You will need a One Time Password(OTP).The OTP is " + myotp, "UTF-8"));
                    smsurl.append("&response=Y");
                    String from = "no-reply@bennett.edu.in";
                    String to = dep.get("pemailid")==null?"":dep.get("pemailid").toString();
                    // String to = "s.lucky0393@gmail.com,vishnu488993@gmail.com";
                    String subject = "Bennett University";
                    String body = "To Login You will need a One Time Password(OTP).The OTP is " + myotp;
                    List sysalert = new ArrayList();
                    mailSender.sendSms(smsurl.toString());
                    if (mailSender.sendMail(from, to, subject, body)) {
                        sysalert.add("OTP send SMS And Emailid ");
                    } else {
                        sysalert.add("Email send fail");
                    }
                    logger.info("OTP send = "+myotp);
                    return myotp;
                } catch (UnsupportedEncodingException ex) {
                    myotp = "0000";
                    logger.error("Proble in otp==" + conlist);
                }
            } else {
                return myotp;
            }
        }
        return myotp;
    }

    public int getHostelOutStuCancellation(Map hostelstu) {
         logger.info("HostelOutStuCancellation >>>> "+hostelstu);
        return oracelDao.getHostelOutStuCancellation(hostelstu);
    }

    public int getHOParentApproval(Map hostelstu) {
        hostelstu.put("requestdatetime", sdfDate.format(new Date()));
        logger.info("ParentApproval >>>> "+hostelstu);
        return oracelDao.getHOParentApproval(hostelstu);
    }

    public int getHOWardenApproval(Map hostelstu) {
        LinkedCaseInsensitiveMap attsub = (LinkedCaseInsensitiveMap) sessionmap.get(hostelstu.get("refreshtoken").toString());
        hostelstu.put("requestdatetime", sdfDate.format(new Date()));
        hostelstu.put("approvedbymemberid", attsub.get("employeeid"));
        logger.info("WardenApproval >>>> "+hostelstu);
        return oracelDao.getHOWardenApproval(hostelstu);
    }

    public String getHOQrCode(Map refreshtocken) {
        StringBuilder qrString = new StringBuilder();
        try {
            if (sessionmap.containsKey(refreshtocken.get("refreshtoken").toString())) {
                refreshtocken.put("studentid", ((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken.get("refreshtoken"))).get("studentid").toString());
                qrString.append(refreshtocken.get("requestid")).append("@").append(refreshtocken.get("studentid"));
                logger.info("HOQrCode >>>> "+qrString);
                return eds.encrypt(qrString.toString());
            } else {
                return qrString.toString();
            }
        } catch (Exception e) {
            return qrString.toString();
        }
    }
//    public String getHOQrCode(Map refreshtocken) {
//        StringBuilder qrString = new StringBuilder();
//        try {
//            if (sessionmap.containsKey(refreshtocken.get("refreshtoken").toString())) {
//                refreshtocken.put("studentid", ((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken.get("refreshtoken"))).get("studentid").toString());
//                Map hosteldetail = (LinkedCaseInsensitiveMap) oracelDao.getHOQrCode(refreshtocken).get(0);
//                Map studetail = (LinkedCaseInsensitiveMap) oracelDao.getEmergencyStudentdetail(refreshtocken).get(0);
//                qrString.append("Enrollmentno :").append(studetail.get("enrollmentno")).append("\n");
//                qrString.append("Name :").append(studetail.get("name")).append("\n");
//                qrString.append("Program :").append(studetail.get("programcode")).append("\n");
//                qrString.append("Check Out :").append(hosteldetail.get("checkoutdate")).append("\n");
//                qrString.append("Check IN :").append(hosteldetail.get("checkindate")).append("\n");
//                qrString.append("Place of Visit :").append(hosteldetail.get("pleasewherevisited")).append("\n");
//                qrString.append("Warden Approval :").append(hosteldetail.get("status")).append("\n");
//                qrString.append("Parents Approval :").append(hosteldetail.get("parentapprovalstatus"));
//
//                return qrString.toString();
//            } else {
//                return qrString.toString();
//            }
//        } catch (Exception e) {
//            return qrString.toString();
//        }
//    }

    public Map getStuMarks(String refreshtocken) {
        Map finalmarksmap = new HashMap();
        try {
            Map submark = new HashMap();;
            List markslist = new ArrayList();
            Set subcodelist = new HashSet();
            if (sessionmap.containsKey(refreshtocken)) {
                List l = oracelDao.getStuMarks((LinkedCaseInsensitiveMap) sessionmap.get(refreshtocken));
                for (int i = 0; i < l.size(); i++) {
                    subcodelist.add(((LinkedCaseInsensitiveMap) l.get(i)).get("SUBJECTCODE").toString());
                }
                List<String> deDupStringList = new ArrayList<>(new HashSet<>(subcodelist));
                for (int j = 0; j < deDupStringList.size(); j++) {
                    String subcode = deDupStringList.get(j);

                    Map subtotmark = new HashMap();
                    subtotmark.put("weightage", "0.0");
                    subtotmark.put("marksawarded", "0.0");
                    List markdetail = new ArrayList();
                    for (int i = 0; i < l.size(); i++) {
                        Map submarkdetail = new HashMap();
                        Map curr = ((LinkedCaseInsensitiveMap) l.get(i));
                        if (deDupStringList.get(j).equals(curr.get("SUBJECTCODE").toString())) {
                            subtotmark.put("subjectcode", curr.get("SUBJECTCODE").toString());
                            subtotmark.put("subjectdesc", curr.get("SUBJECTDESC").toString());
                            String weightage = curr.get("WEIGHTAGE") == null ? "0.0" : curr.get("WEIGHTAGE").toString();
                            double totweight = Double.parseDouble(subtotmark.get("weightage").toString()) + Double.parseDouble(weightage);
                            subtotmark.put("weightage", df.format(totweight));
                            String finalmark = curr.get("finalmark") == null ? "0.0" : curr.get("finalmark").toString();
                            double totawarded = Double.parseDouble(subtotmark.get("marksawarded").toString()) + Double.parseDouble(finalmark);
                            subtotmark.put("marksawarded", df.format(totawarded));
                            submarkdetail.put("eventcode", curr.get("exameventcode").toString());
                            submarkdetail.put("weightage", (curr.get("WEIGHTAGE") == null ? curr.get("MARKSENTRYPROMPT2").toString() : df.format(Double.parseDouble(weightage))));
                            submarkdetail.put("marksawarded", df.format(Double.parseDouble(finalmark)));
                            markdetail.add(submarkdetail);
                        }
                    }
                    submark.put(subcode, markdetail);
                    markslist.add(subtotmark);
                }
                finalmarksmap.put("marksdetail", submark);
                finalmarksmap.put("totmarkslist", markslist);
            }
        } catch (Exception e) {
            logger.error("problem in getStuMarks " + refreshtocken);
        }

        return finalmarksmap;
    }
//    public static void main(String[] args) {
//        MobileOracelService mos = new MobileOracelService();
//        mos.getLoginResponse("AG40651", "Hello@123", "S");
//        System.out.println(mos.getLoginDetail("==QfiMjMxA0bsxWZIJiOiQmcvd3czFGciwiI2ADNHFkI6ISZtFmbyV2c1Jye"));
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bennett.mobapi.util;

import com.bennett.mobapi.controller.MobileController;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author Yatendra Singh
 */
@Component
@PropertySource("classpath:ldapad.properties")
public class LdapActiveDirectory {
static final org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger(LdapActiveDirectory.class);
    
    @Value("${ldap.factory}")
    private String factory;

    @Value("${ldap.security}")
    private String security;

    @Value("${ldap.principal}")
    private String principal;

    @Value("${ldap.url}")
    private String url;

    @Value("${ldap.ssearchBase}")
    private String ssearchBase;

    @Value("${ldap.fsearchBase}")
    private String fsearchBase;

    @Value("${ldap.gsearchBase}")
    private String gsearchBase;

    @Value("${ldap.wsearchBase}")
    private String wsearchBase;

    public Map getLDAPValidate(Map user) {
        String searchBase = "";
        
        Map mp = new HashMap();
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, factory);
        env.put(Context.SECURITY_AUTHENTICATION, security);
        env.put(Context.SECURITY_PRINCIPAL, (user.get("userid").toString().contains("@")? user.get("userid").toString():user.get("userid").toString() + principal));
        env.put(Context.SECURITY_CREDENTIALS, user.get("password").toString());
        env.put(Context.PROVIDER_URL, url);
        DirContext ctx;

        try {
            ctx = new InitialDirContext(env);
            if (user.get("utype").equals("S")) {
                searchBase = ssearchBase;
            } else if (user.get("utype").equals("E")) {
                searchBase = fsearchBase;
            } else if (user.get("utype").equals("P")) {
                searchBase = gsearchBase;
            } else if (user.get("utype").equals("W")) {
                searchBase = wsearchBase;
            }
            String searchFilter = "(&(objectClass=person)(sAMAccountName="+user.get("userid").toString()+")(userPrincipalName="+user.get("userid").toString()+"@bennett.edu.in))";
            SearchControls sCtrl = new SearchControls();
            sCtrl.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration answer = ctx.search(searchBase, searchFilter, sCtrl);
         //   boolean pass = false;
//            if (answer.hasMoreElements()) {
//                pass = true;
//            }
//            if (pass) {
//                System.out.println("1111>>>" + pass);
//            } else {
//                System.out.println("2222>>>" + pass);
//            }
             mp.put("uid",user.get("userid").toString());
            logger.info("Login "+(new Date())+"=="+user.get("userid").toString());
            while (answer.hasMore()) {
                SearchResult result = (SearchResult) answer.next();
                Attributes attribs = result.getAttributes();
                NamingEnumeration<String> attribsIDs = attribs.getIDs();
                while (attribsIDs.hasMore()) {
                    String attrID = attribsIDs.next();
                    NamingEnumeration values = ((BasicAttribute) attribs.get(attrID)).getAll();
                    //System.out.println("Naming Enumertaion Values   :: " + attrID + "  =" + values.next().toString());
                    mp.put(attrID, values.next().toString());
                }
            }
           // System.out.println(">>>" + mp);

        } catch (NamingException ex) {
               logger.error("LdapActiveDirectory  problem in Ldap Login with user : = "+user );
            mp = new HashMap();
        }
        return mp;
    }

}

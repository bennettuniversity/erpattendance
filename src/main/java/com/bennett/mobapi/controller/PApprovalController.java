/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bennett.mobapi.controller;

import com.bennett.mobapi.service.PApprovalService;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Yatendra Singh
 */
@Controller
@RequestMapping("/v2")
public class PApprovalController {

    static final Logger logger = Logger.getLogger(MobileController.class);

    @Autowired
    PApprovalService PAService;
    
    
    @RequestMapping(value = "/murl/{reshtoken}", method = RequestMethod.GET)
    public @ResponseBody
    String getParentApprovalReq(@PathVariable String reshtoken, HttpServletResponse response) {
        StringBuilder sb = new StringBuilder();
        try {
          return PAService.getParentStatus(reshtoken);
        } catch (Exception e) {
            logger.error("PApprovalController :: getParentApprovalReq ::  /"+reshtoken);
        }
        return sb.toString();
    }

    @RequestMapping(value = "/parentapv", method = RequestMethod.POST)
    public @ResponseBody
    String getUpdateApproval(@RequestBody Map mp, HttpServletResponse response) {
        StringBuilder sb = new StringBuilder();
        try {
           PAService.getHOParentApproval(mp);
            logger.info(" parent approval, "+mp);
        } catch (Exception e) {
            logger.error("Problem in  parent approval >>>> getUpdateApproval ::  "+mp);
        }
        return sb.toString();
    }

}

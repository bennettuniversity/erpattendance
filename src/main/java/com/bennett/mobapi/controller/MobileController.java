/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bennett.mobapi.controller;

import com.bennett.mobapi.service.MobileOracelService;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Yatendra Singh
 */
@Controller
@RequestMapping("/mobapi")
public class MobileController {

    static final Logger logger = Logger.getLogger(MobileController.class);

    @Autowired
    MobileOracelService oracelService;

    @RequestMapping(value = "/attendancedetail", method = RequestMethod.POST)
    public @ResponseBody
    List getAttendanceDetail(@RequestBody String reshtoken) {
        try {
            return oracelService.getAttendanceDetail(reshtoken);
        } catch (Exception e) {
             logger.error("MobileController :: getAttendanceDetail :: ");
            return new ArrayList();
        }

    }
    
    
    @RequestMapping(value = "/refreshtoken", method = RequestMethod.POST)
    public @ResponseBody
    String getRefreshToken(@RequestBody Map studata) {
        try {
            return oracelService.getRefreshToken(studata);
        } catch (Exception e) {
            logger.error("MobileController :: getRefreshToken :: ");
            return "false"; 
        }

    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public @ResponseBody
    Map getloginDetail(@RequestBody  String login) {
        try {
            return oracelService.getLoginDetail(login);
        } catch (Exception e) {
            logger.error("MobileController :: getRefreshToken :: Problem in login  "+login);
            return new HashMap(); 
        }

    }
    
    
    @RequestMapping(value = "/desrefreshtoken", method = RequestMethod.POST)
    public @ResponseBody
    boolean getRefreshTokenDestroy(@RequestBody String reshtoken) {
        try {
            return oracelService.getRefreshTokenDestroy(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getRefreshTokenDestroy :: ");
            return false;
        }

    }
    @RequestMapping(value = "/mobilemenu", method = RequestMethod.POST)
    public @ResponseBody
     List getMobileMenu(@RequestBody String reshtoken) {
        try {
            return oracelService.getMobileMenu(reshtoken);
        } catch (Exception e) {
           logger.error("MobileController :: getMobileMenu :: ");
        }
        return new ArrayList();

    }
    @RequestMapping(value = "/foodmenu", method = RequestMethod.POST)
    public @ResponseBody
     List getFoodMenu(@RequestBody String reshtoken) {
        try {
            return oracelService.getFoodMenu(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getFoodMenu :: ");
            
        }
        return new ArrayList();

    }
    
    
    @RequestMapping(value = "/getprofile/{reshtoken}", method = RequestMethod.GET)
    public @ResponseBody
    void getUserProfilePic(@PathVariable String reshtoken, HttpServletResponse response) {
        try {
            response.setContentType("image/jpeg");
            byte[] b = (byte[]) oracelService.getUserProfile(reshtoken);
            if (b != null) {
                InputStream in = new ByteArrayInputStream(b);
                BufferedImage bImage = ImageIO.read(in);
                OutputStream out = response.getOutputStream();
                ImageIO.write(bImage, "jpg", out);
                out.close();
            }
        } catch (Exception e) {
              logger.error("MobileController :: getUserProfilePic :: Proble in image ");
        }

    }
    @RequestMapping(value = "/attendancesubdetail", method = RequestMethod.POST)
    public @ResponseBody
    List getAttendanceSubDetail(@RequestBody Map subid) {
        try {
            System.out.println(" attendancedetail  "+subid);
            return oracelService.getAttendanceSubDetail(subid);
        } catch (Exception e) {
            logger.error("MobileController :: getAttendanceSubDetail :: ");
            return new ArrayList();
        }

    }

    @RequestMapping(value = "/stynumberresult", method = RequestMethod.POST)
    public @ResponseBody
    List getStynumberResult(@RequestBody String reshtoken) {
        try {
            return oracelService.getStynumberResult(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getStynumberResult :: ");
            return new ArrayList();
        }

    }
    @RequestMapping(value = "/stynumberbyresult", method = RequestMethod.POST)
    public @ResponseBody
    List getStynumberByResult(@RequestBody Map subid) {
        try {
            return oracelService.getStynumberByResult(subid);
        } catch (Exception e) {
            logger.error("MobileController :: getStynumberByResult :: ");
            return new ArrayList();
        }

    }
    @RequestMapping(value = "/timetabledetail", method = RequestMethod.POST)
    public @ResponseBody
    List getTimeTable(@RequestBody  String reshtoken) {
        try {
            return oracelService.getTimeTable(reshtoken);
        } catch (Exception e) {
              logger.error("MobileController :: getTimeTable :: ");
            return new ArrayList();
        }

    }
    
    @RequestMapping(value = "/emergencyservice", method = RequestMethod.POST)
    public @ResponseBody
    List getEmergencyService(@RequestBody  String reshtoken) {
        try {
            return oracelService.getEmergencyService(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getEmergencyService :: ");
            return new ArrayList();
        }

    }
    @RequestMapping(value = "/emergencyalertservice", method = RequestMethod.POST)
    public @ResponseBody
    List getEmergencyService(@RequestBody  Map reshtoken) {
        try {
            return oracelService.getEmergencyAlertService(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getEmergencyService :: ");
            return new ArrayList();
        }

    }
    @RequestMapping(value = "/studentprofiledata", method = RequestMethod.POST)
    public @ResponseBody
    List getStudentProfileData(@RequestBody  String reshtoken) {
        try {
            return oracelService.getStudentProfileData(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getStudentProfileData :: ");
            return new ArrayList();
        }

    }
	
    @RequestMapping(value = "/mobilemenuorder", method = RequestMethod.POST)
    public @ResponseBody
    List getMobileMenuOrder(@RequestBody Map reshtoken) {
        try {
            return oracelService.getMobileMenuOrder(reshtoken);
        } catch (Exception e) {
             logger.error("MobileController :: getMobileMenuOrder :: ");
            return new ArrayList();
        }
    }
    
    @RequestMapping(value = "/hosteloutpurpose", method = RequestMethod.POST)
    public @ResponseBody
    Map getHostelOutPurpose(@RequestBody String reshtoken) {
        try {
            return oracelService.getHostelOutPurpose(reshtoken);
        } catch (Exception e) {
             logger.error("MobileController :: getMobileMenuOrder :: ");
            return new HashMap();
        }
    }
    
    @RequestMapping(value = "/hosteloutstudetail", method = RequestMethod.POST)
    public @ResponseBody
    int getHostelOutStuDetail(@RequestBody Map hostelstu) {
        try {
            return oracelService.getHostelOutStuDetail(hostelstu);
        } catch (Exception e) {
             logger.error("MobileController :: getMobileMenuOrder :: ");
            return 0;
        }
    }
    
    @RequestMapping(value = "/sturequeststatus", method = RequestMethod.POST)
    public @ResponseBody
    List getStudentRequestStatus(@RequestBody String reshtoken) {
        try {
            return oracelService.getStudentRequestStatus(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getMobileMenuOrder :: ");
            return new ArrayList();
        }
    }
    @RequestMapping(value = "/getnewotp", method = RequestMethod.POST)
    public @ResponseBody
    String getNewOtp(@RequestBody String reshtoken) {
        try {
            return oracelService.getNewOtp(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getMobileMenuOrder :: ");
            return "0000";
        }
    }
    
    @RequestMapping(value = "/hostucancellation", method = RequestMethod.POST)
    public @ResponseBody
    int  getHostelOutStuCancellation(@RequestBody Map reshtoken) {
        try {
            return oracelService.getHostelOutStuCancellation(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getHostelOutStuCancellation :: ");
            return 0;
        }
    }
    
    @RequestMapping(value = "/hoparentapproval", method = RequestMethod.POST)
    public @ResponseBody
    int getHOParentApproval(@RequestBody Map reshtoken) {
        try {
            return oracelService.getHOParentApproval(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getHOParentApproval :: ");
            return 0;
        }
    }
    
    @RequestMapping(value = "/howardenapproval", method = RequestMethod.POST)
    public @ResponseBody
    int getHOWardenApproval(@RequestBody Map reshtoken) {
        try {
           return oracelService.getHOWardenApproval(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getHOWardenApproval :: ");
            return 0;
        }
    }
    @RequestMapping(value = "/hoqrcode", method = RequestMethod.POST)
    public @ResponseBody
    String getHOQrCode(@RequestBody Map reshtoken) {
        try {
           return oracelService.getHOQrCode(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getHOQrCode :: ");
            return "";
        }
    }
    
    @RequestMapping(value = "/getmarks", method = RequestMethod.POST)
    public @ResponseBody
    Map getStuMarks(@RequestBody String reshtoken) {
        try {
            return oracelService.getStuMarks(reshtoken);
        } catch (Exception e) {
            logger.error("MobileController :: getHOQrCode :: ");
            return new HashMap();
        }
    }
    
    
    
    
}



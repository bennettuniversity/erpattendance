/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bennett.mobapi.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.jboss.logging.Logger;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedCaseInsensitiveMap;

/**
 *
 * @author Yatendra Singh
 */
@Repository
public class MobileOracelDao {

    static final Logger logger = org.jboss.logging.Logger.getLogger(MobileOracelDao.class);
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
    
    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List getAttendanceDetail(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from V#STUDENTATTENDANCE where STUDENTID='").append(studetail.get("studentid")).append("' and INSTITUTEID ='").append(studetail.get("instituteid")).append("' and REGISTRATIONID='" + getAttendanceRegNo(studetail.get("studentid").toString()) + "'");
        try {
            return jdbcTemplate.queryForList(sb.toString());

        } catch (Exception ex) {
            logger.error("MobileOracelDao  getAttendanceDetail:: " + studetail, ex);
        }
        return new ArrayList();
    }

    public List getRefreshToken(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select p.instituteid as instituteid, p.studentid as studentid,  p.stynumber as stynumber ");
        sb.append(" from studentmaster p");
        sb.append(" where p.studentid='").append(studetail.get("studentid")).append("'");
        return jdbcTemplate.queryForList(sb.toString());
    }

    public List getRefreshTokenForEmployee(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select p.companyid as companyid, p.employeeid as employeeid");
        sb.append(" from employeemaster p");
        sb.append(" where p.employeeid='").append(studetail.get("studentid")).append("'");
        return jdbcTemplate.queryForList(sb.toString());
    }

    public byte[] getUserProfilePic(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append("select photo from studentphoto where studentid = '").append(studetail.get("studentid")).append("' ");
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sb.toString());
        for (Map<String, Object> row : rows) {
            try {
                byte[] b = (byte[]) row.get("photo");
                return b;
            } catch (Exception ex) {
                logger.error(" Problem in MobileOracelDao  getUserProfilePic:: " + studetail);
            }
        }
        return null;
    }

    public List getMobileMenu() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("select  * from ma#rightsmaster");
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getAttendanceSubDetail  :: ", e);
        }
        return new ArrayList();
    }

    public List getFoodMenu() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select MENUDAY ,MENUTYPE, LISTAGG (ITEAMDESC,'$')  WITHIN GROUP (ORDER BY SEQID)  menu ");
            sb.append("from MAPP#FOODMENU app ");
            sb.append("where MENUDATE in (select max(MENUDATE) from MAPP#FOODMENU where MENUTYPE ='B') ");
            sb.append("and MENUTYPE ='B' ");
            sb.append("group by MENUDAY ,MENUTYPE ");
            sb.append("union ");
            sb.append("select MENUDAY ,MENUTYPE, LISTAGG (ITEAMDESC,'$')  WITHIN GROUP (ORDER BY SEQID)  menu ");
            sb.append("from MAPP#FOODMENU app ");
            sb.append("where MENUDATE in (select max(MENUDATE) from MAPP#FOODMENU where MENUTYPE ='L') ");
            sb.append("and MENUTYPE ='L' ");
            sb.append("group by MENUDAY ,MENUTYPE ");
            sb.append("union ");
            sb.append("select MENUDAY ,MENUTYPE, LISTAGG (ITEAMDESC,'$')  WITHIN GROUP (ORDER BY SEQID)  menu ");
            sb.append("from MAPP#FOODMENU app ");
            sb.append("where MENUDATE in (select max(MENUDATE) from MAPP#FOODMENU where MENUTYPE ='D') ");
            sb.append("and MENUTYPE ='D' ");
            sb.append("group by MENUDAY ,MENUTYPE ");
            sb.append("order by 1 ");
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getAttendanceSubDetail ::  ", e);
        }
        return new ArrayList();
    }

    public List getAttendanceSubDetail(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select ");
        sb.append(" datetime,");
        sb.append(" nvl(PRESENT,'Absent') as attper");
        sb.append(" from ");
        sb.append(" V#dAYWISEATTENDANCE where ");
        sb.append(" INSTITUTEID='").append(studetail.get("instituteid")).append("' ");
        sb.append(" and REGISTRATIONID='").append(getAttendanceRegNo(studetail.get("studentid").toString())).append("' ");
        sb.append(" and STUDENTID='").append(studetail.get("studentid")).append("' ");
        sb.append(" and SUBJECTID='").append(studetail.get("subid").toString().split("@@")[0]).append("' ");
        sb.append(" and SUBJECTCOMPONENTID='").append(studetail.get("subid").toString().split("@@")[1]).append("'");
        sb.append(" order by TO_DATE(substr(datetime,0,10),'DD/MM/YYYY'),substr(datetime,19,2) ,substr(datetime,13,2)");

        try {
            return jdbcTemplate.queryForList(sb.toString());

        } catch (Exception e) {
            logger.error("MobileOracelDao :: getAttendanceSubDetail :: " + studetail + " :: ", e);
        }
        return new ArrayList();
    }

    public List getStynumberResult(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from V#STUDENTSGPACGPA where INSTITUTEID ='").append(studetail.get("instituteid")).append("' and STUDENTID='").append(studetail.get("studentid")).append("'");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getStynumberResult :: " + studetail + " :: ", e);
        }
        return new ArrayList();
    }

    public List getStynumberByResult(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from V#STUDENTRESULTDETAIL where INSTITUTEID ='").append(studetail.get("instituteid")).append("' and STUDENTID='").append(studetail.get("studentid")).append("' and stynumber = '").append(studetail.get("sysnum")).append("'");
        try {
            return jdbcTemplate.queryForList(sb.toString());

        } catch (Exception e) {
            logger.error("MobileOracelDao :: getStynumberByResult :: " + studetail + " :: ", e);
        }
        return new ArrayList();
    }

    public List getTimeTable(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select REGISTRATIONID, f.ALLOCATIONDAY  as day,  f.FROMSESSIONTIME||' to '||f.TOSESSIONTIME as datetime,  a.SHORTNAME , e.SUBJECTCOMPONENTCODE ,  d.SUBJECTCODE ,  c.ROOMCODE, a.EMPLOYEENAME ");
        sb.append(" from  V#STAFF a,    ROOMMASTER c, SUBJECTMASTER d,  SUBJECTCOMPONENT e,  TT#TIMETABLEALLOCATION f ");
        sb.append(" where (f.TTREFERENCEID in (select  g.TTREFERENCEID   from  FACULTYSUBJECTTAGGING g ");
        sb.append(" where (g.FSTID in (  select  h.FSTID   from  FACULTYSTUDENTTAGGING h ");
        sb.append(" where  h.INSTITUTEID='").append(studetail.get("instituteid")).append("'   and h.STUDENTID='").append(studetail.get("studentid")).append("')) ");
        sb.append(" and g.INSTITUTEID='").append(studetail.get("instituteid")).append("'   group by  g.TTREFERENCEID)) ");
        sb.append(" and f.REGISTRATIONID='").append(getTimetableRegNo(studetail.get("studentid").toString())).append("' ");
        sb.append(" and f.INSTITUTEID='").append(studetail.get("instituteid")).append("'");
        sb.append(" and f.STAFFID=a.EMPLOYEEID ");
        sb.append(" and f.SUBJECTID=d.SUBJECTID ");
        sb.append(" and f.INSTITUTEID=d.INSTITUTEID");
        sb.append(" and f.ROOMID=c.ROOMID");
        sb.append(" and f.INSTITUTEID=c.INSTITUTEID");
        sb.append(" and e.SUBJECTCOMPONENTID=f.SUBJECTCOMPONENTID");
        sb.append(" and e.INSTITUTEID=f.INSTITUTEID ");
        sb.append(" order by  substr(f.FROMSESSIONTIME,  7,  8),  f.FROMSESSIONTIME");

        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getTimeTable :: " + studetail + " :: ", e);
        }
        return new ArrayList();
    }

    public List getEmergencyService(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT DEPARTMENT,   emailsubject,  ");
        sb.append(" listagg(CONTECTNO,',') within group (order by slno) contectno, ");
        sb.append(" listagg(emailid,',') within group (order by slno) emailid ");
        sb.append(" FROM MAPP#EMERGENCY ");
        sb.append(" where nvl(deactive,'N')='N' ");
        sb.append(" group by DEPARTMENT,  emailsubject ");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getEmergencyService :: " + studetail + " :: ", e);
        }
        return new ArrayList();
    }

    public List getEmergencyStudentdetail(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append("  select enrollmentno,name,programcode||', '||branchcode ||', '||a.STYNUMBER as programcode , ");
        sb.append(" f.STUDENTPERSONALEMAILID, f.SCELLNO, ");
        sb.append(" a.ENROLLMENTNO, a.ROLLNUMBER, ");
        sb.append(" (select g.HOSTELCODE ");
        sb.append(" from hostelmaster g, studenthosteldetail h, HostelRoomMaster i ");
        sb.append(" where g.HostelID=h.HostelID ");
        sb.append(" and a.StudentID=h.Studentid ");
        sb.append(" and h.HOSTELID=i.hostelID ");
        sb.append(" and h.ROOMID=i.roomid ");
        sb.append(" and rownum=1 ");
        sb.append(" ) hostelcode, ");
        sb.append(" (select i.roomCODE ");
        sb.append(" from hostelmaster g, studenthosteldetail h, HostelRoomMaster i ");
        sb.append(" where g.HostelID=h.HostelID ");
        sb.append(" and a.StudentID=h.Studentid ");
        sb.append(" and h.HOSTELID=i.hostelID ");
        sb.append(" and h.ROOMID=i.roomid ");
        sb.append(" and rownum=1 ");
        sb.append(" ) roomcode ");
        sb.append(" from studentmaster a,  InstituteMaster c, programmaster d,branchmaster e,studentphone f, sectionmaster g, programwisesubsection h ");
        sb.append(" where a.instituteid=c.instituteid ");
        sb.append(" and a.instituteid=d.instituteid ");
        sb.append(" and a.programid=d.programid ");
        sb.append(" and a.instituteid=e.instituteid ");
        sb.append(" and a.programid=e.programid ");
        sb.append(" and a.branchid=e.branchid ");
        sb.append(" and a.studentid=f.studentid ");
        sb.append(" and a.instituteid=g.instituteid ");
        sb.append(" and a.sectionid=g.sectionid ");
        sb.append(" and a.instituteid=h.instituteid ");
        sb.append(" and a.sectionid=h.sectionid ");
        sb.append(" and a.subsectionid=h.subsectionid ");
        sb.append(" and a.stynumber=h.stynumber ");
        sb.append(" and a.studentid  ='").append(studetail.get("studentid")).append("' ");
        sb.append(" and nvl(a.activestatus,'N')='A' ");

        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (DataAccessException e) {
            logger.error("MobileOracelDao :: getEmergencyStudentdetail :: " + studetail + " :: ", e);
        }
        return new ArrayList();
    }

    public List getEmergencyEmployeeDetail(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select a.employeecode,a.employeename from employeemaster a where  a.employeeid  ='").append(studetail.get("EMPLOYEEID")).append("' ");
        sb.append(" and nvl(a.deactive,'N')='N' ");

        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (DataAccessException e) {
            logger.error("MobileOracelDao :: getEmergencyStudentdetail :: " + studetail + " :: ", e);
        }
        return new ArrayList();
    }

    public List getDepartmentEmergencyService(Map dep) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT department,emailsubject,mailtext,smstext,emailsubject, ");
        sb.append(" listagg(contectno,',') within group (order by slno) contectno, ");
        sb.append(" listagg(emailid,',') within group (order by slno) emailid ");
        sb.append(" FROM MAPP#EMERGENCY ");
        sb.append(" where nvl(deactive,'N')='N'  and DEPARTMENT='").append(dep.get("departmentcode")).append("'");
        sb.append(" group by department,emailsubject,mailtext,smstext,emailsubject ");
        try {
            List li = jdbcTemplate.queryForList(sb.toString());
            return li;
        } catch (DataAccessException e) {
            logger.error("MobileOracelDao :: getDepartmentEmergencyService :: " + dep + " :: ", e);
        }
        return new ArrayList();
    }

    public List getMobileMenuOrder(String type) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT RIGHTSID as rightsid,RIGHTSINFO as iconlabel,NVL(url,'N') as url FROM MA#RIGHTSMASTER WHERE relatedto like '%").append(type.toUpperCase()).append("%'  and deactive = 'N' order by seqid ");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (DataAccessException e) {
            logger.error("MobileOracelDao :: getMobileMenuOrder :: ");
        }
        return new ArrayList();
    }

    public List getUserType(Map type) {
        StringBuilder sb = new StringBuilder();
        // sb.append(" SELECT RIGHTSID as rightsid,RIGHTSINFO as iconlabel,NVL(url,'N') as url FROM MA#RIGHTSMASTER WHERE relatedto like '%" + type.toUpperCase() + "%'  and deactive = 'N' order by seqid ");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getMobileMenuOrder :: ");
        }
        return new ArrayList();
    }

    public List getHostelOutPurpose() {
        StringBuilder sb = new StringBuilder();
        sb.append("select hosteloutp.hosteloutpurpose as hosteloutpurpose from   hosteloutpurposemaster hosteloutp where  nvl(hosteloutp.decative, 'N')='N' order by  hosteloutp.seqid");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getMobileMenuOrder :: ");
        }
        return new ArrayList();
    }

    public void saveHostelRequest() {
//        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("GenerateID");
//        Map<String, Object> inParamMap = new HashMap<String, Object>();
//        inParamMap.put("mInstITUTEID", "BUINTA0000001");
//        inParamMap.put("pIDCODE", "EXM");
//        SqlParameterSource in = new MapSqlParameterSource(inParamMap);
//        String simpleJdbcCallResult = simpleJdbcCall.executeFunction(String.class,in);
//        System.out.println(simpleJdbcCallResult);

//      SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withFunctionName("generateIDS");
//      SqlParameterSource paramMap = new MapSqlParameterSource()
//              .addValue("mInstITUTEID","BUINTA0000001",OracleTypes.VARCHAR)
//              .addValue("pIDCODE", "HOSTELREQUESTID",OracleTypes.VARCHAR);
//
//      String sum = call.executeFunction(String.class, paramMap);
//      System.out.println(sum);
    }

    public int getHostelOutStuDetail(Map hostelstu) {
        int status = 0;
        String hostalid = "BENNHRQT00000000001";
        StringBuilder stb = new StringBuilder();
        //select 'BENHRMOB'||lpad(substr(max(REQUESTID),9,11)+1,11,0) hostalid from  HOSTELOUTGOINGREQUEST where REQUESTid like'BENHRMOB%'
        stb.append(" select max(requestno) hostalid from (select 'BENNHRQT'||(select to_char(sysdate, 'YYMM') from dual)||(select lpad(substr(max(REQUESTID),14)+1,8,0) FROM DUAL) requestno ");
        stb.append(" from HOSTELOUTGOINGREQUEST ");
        stb.append(" WHERE   ");
        stb.append(" substr(REQUESTID,9,2) = to_char(sysdate, 'YY') ");
        stb.append(" AND substr(REQUESTID,11,2) = to_char(sysdate, 'mm') ");
        stb.append("  union  ");
        stb.append(" select   'BENNHRQT'||(select to_char(sysdate, 'YYMM') from dual)||'00000001' requestno ");
        stb.append(" from HOSTELOUTGOINGREQUEST ");
        stb.append(" where ( substr(REQUESTID,9,2) = to_char(sysdate, 'YY') or substr(REQUESTID,11,2) <> to_char(sysdate, 'mm') )) ");
        try {
            hostalid = ((LinkedCaseInsensitiveMap) (jdbcTemplate.queryForList(stb.toString())).get(0)).get("hostalid").toString();
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getHostelOutStuDetail :: " + hostelstu);
        }
        String hostalno = "HRMOB00000001";
        StringBuilder st = new StringBuilder();
        //select 'HRMOB'||lpad(substr(max(REQUESTNO),6,8)+1,8,0) requestno from  HOSTELOUTGOINGREQUEST where REQUESTNO like'HRMOB%'
            st.append(" select max(requestno) requestno from ( ");
            st.append(" select 'HLNO'||(select to_char(sysdate, 'YYYYMM') from dual)||lpad((select substr(max(REQUESTNO),11,5)+1 FROM DUAL),5,0) requestno ");
            st.append(" from HOSTELOUTGOINGREQUEST  ");
            st.append(" WHERE substr(REQUESTNO,5,4) = to_char(sysdate, 'YYYY')  ");
            st.append(" AND substr(REQUESTNO,9,2) = to_char(sysdate, 'mm') ");
            st.append("  union    ");
            st.append(" select distinct 'HLNO'||(select to_char(sysdate, 'YYYYMM') from dual)||'0001' requestno  ");
            st.append(" from HOSTELOUTGOINGREQUEST  ");
            st.append(" where  substr(REQUESTNO,9,2) <> to_char(sysdate+35, 'mm') )");
//        st.append(" select max(requestno) requestno from (select 'HLNO'||(select to_char(sysdate, 'YYYYMM') from dual)||(select substr(max(REQUESTNO),11,5)+1 FROM DUAL) requestno ");
//        st.append(" from HOSTELOUTGOINGREQUEST  ");
//        st.append(" WHERE   substr(REQUESTNO,5,4) = to_char(sysdate, 'YYYY')  ");
//        st.append(" AND substr(REQUESTNO,9,2) = to_char(sysdate, 'mm')  ");
//        st.append("  union   ");
//        st.append(" select  'HLNO'||(select to_char(sysdate, 'YYYYMM') from dual)||'0001' requestno  ");
//        st.append(" from HOSTELOUTGOINGREQUEST  ");
//        st.append(" where substr(REQUESTNO,9,2) <> to_char(sysdate, 'mm') ) ");
        try {
            hostalno = ((LinkedCaseInsensitiveMap) (jdbcTemplate.queryForList(st.toString())).get(0)).get("requestno").toString();
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getHostelOutStuDetail :: " + hostelstu);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO HOSTELOUTGOINGREQUEST(requestid,requestno,studentid,checkoutdatetime,expactedcheckindatetime,pleasewherevisited,purposeofvisit,requestremarks,hostelid,roomid,status,requestdatetime,requestbymemberid,requestbymembertype)");
        sb.append("VALUES(");
        sb.append("'").append(hostalid).append("'");
        sb.append(",'").append(hostalno).append("'");
        sb.append(",'").append(hostelstu.get("studentid")).append("'");
        sb.append(",TO_DATE('").append(hostelstu.get("checkoutdatetime")).append("', 'dd/mm/yyyy hh24:mi:ss')");
        sb.append(",TO_DATE('").append(hostelstu.get("expactedcheckindatetime")).append("', 'dd/mm/yyyy hh24:mi:ss')");
        sb.append(",'").append(hostelstu.get("pleasewherevisited")).append("'");
        sb.append(",'").append(hostelstu.get("purposeofvisit")).append("'");
        sb.append(",'").append(hostelstu.get("requestremarks")).append("'");
        sb.append(",'").append(hostelstu.get("hostelid")).append("'");
        sb.append(",'").append(hostelstu.get("roomid")).append("'");
        sb.append(",'").append(hostelstu.get("status")).append("'");
        sb.append(",TO_DATE('").append(hostelstu.get("requestdatetime")).append("', 'dd/mm/yyyy hh24:mi:ss')");
        sb.append(",'").append(hostelstu.get("requestbymemberid")).append("'");
        sb.append(",'").append(hostelstu.get("requestbymembertype")).append("')");
        try {
            status = jdbcTemplate.update(sb.toString());
        } catch (Exception e) {
            status = 0;
            logger.error("MobileOracelDao :: getMobileMenuOrder :: ");
        }
        return status;
    }

    public List getStudentHostelDetail(String studentid) {
        StringBuilder sb = new StringBuilder();
        sb.append("select hostelid,roomid from v#studenthostelallocation where studentid='").append(studentid).append("'");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getMobileMenuOrder :: ");
        }
        return new ArrayList();
    }

    public List getStudentRequestStatus(String studentid) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select * from (select requestid,requestno,status,(TO_CHAR(checkoutdatetime ,'dd/mm/yyyy hh24:mi:ss')) checkoutdate,TO_CHAR(expactedcheckindatetime ,'dd/mm/yyyy hh24:mi:ss') checkindate,nvl(parentapprovalstatus,'P') parentapprovalstatus,nvl(approvalremarks,'No remark') approvalremarks,nvl(parentapprovalremarks,'No remark') parentapprovalremarks from hosteloutgoingrequest ");
        sb.append(" where studentid ='").append(studentid).append("' order by TO_DATE( checkoutdatetime, 'dd/mm/yyyy hh24:mi:ss' ) desc) where rownum <21");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getMobileMenuOrder :: ");
        }
        return new ArrayList();
    }

    public List getWardenRequestStatus() {
        StringBuilder sb = new StringBuilder();
        sb.append(" select hos.requestid,hos.requestno,hos.status,(TO_CHAR(hos.checkoutdatetime ,'dd/mm/yyyy hh24:mi:ss')) checkoutdatetime, (TO_CHAR(hos.checkoutdatetime,'dd/mm/yyyy'))  checkoutdate,");
        sb.append(" TO_CHAR(hos.expactedcheckindatetime ,'dd/mm/yyyy hh24:mi:ss') checkindate,nvl(hos.parentapprovalstatus,'P') parentapprovalstatus,");
        sb.append(" nvl( hos.approvalremarks,'No remark') approvalremarks,");
        sb.append(" nvl(hos.parentapprovalremarks,'No remark') parentapprovalremarks,stu.NAME,stu.ENROLLMENTNO");
        sb.append(" from hosteloutgoingrequest hos,studentmaster stu");
        sb.append(" where hos.studentid=stu.studentid and hos.status ='P' and nvl(hos.parentapprovalstatus,'P') <>'C'  and");
        sb.append(" hos.checkoutdatetime >= sysdate -1");
        sb.append(" order by  trunc(hos.checkoutdatetime),stu.name");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getWardenRequestStatus :: ");
        }
        return new ArrayList();
    }

    public List setStudentCancelStatus(String reruestid) {
        StringBuilder sb = new StringBuilder();
        //sb.append(" select * from (select requestid,requestno,status,(TO_CHAR(checkoutdatetime ,'dd/mm/yyyy hh24:mi:ss')) checkoutdate,TO_CHAR(expactedcheckindatetime ,'dd/mm/yyyy hh24:mi:ss') checkindate,nvl(parentapprovalstatus,'P') parentapprovalstatus from hosteloutgoingrequest ");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: setStudentCancelStatus :: ");
        }
        return new ArrayList();
    }

    public List getMemberDetail(String loginid) {
        StringBuilder sb = new StringBuilder();
        sb.append("select memberid,membertype from V#USERDETAIL where UPPER(loginid)=UPPER('").append(loginid).append("')");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("Problem in MobileOracelDao  getMemberDetail :: " + loginid);
        }
        return new ArrayList();
    }

    public String getTimetableRegNo(String stuid) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT P.REGISTRATIONID ");
        sb.append(" FROM STUDENTREGISTRATION P, REGISTRATIONMASTER Q ");
        sb.append(" WHERE P.INSTITUTEID=Q.INSTITUTEID AND P.REGISTRATIONID=Q.REGISTRATIONID ");
        sb.append(" AND Q.REGISTRATIONDATEFROM=( ");
        sb.append(" SELECT MAX(R.REGISTRATIONDATEFROM) FROM REGISTRATIONMASTER R ");
        sb.append(" WHERE R.REGISTRATIONID IN( SELECT DISTINCT D.REGISTRATIONID ");
        sb.append(" FROM TT#TIMETABLEALLOCATION B, FACULTYSTUDENTTAGGING C, FACULTYSUBJECTTAGGING D ");
        sb.append(" WHERE  B.INSTITUTEID=C.INSTITUTEID AND B.INSTITUTEID=D.INSTITUTEID ");
        sb.append(" AND D.FSTID=C.FSTID ");
        sb.append(" and D.TTREFERENCEID=b.TTREFERENCEID ");
        sb.append(" AND c.STUDENTID='").append(stuid).append("') ");
        sb.append(" AND P.STUDENTID='").append(stuid).append("') ");
        try {
            return ((LinkedCaseInsensitiveMap) (jdbcTemplate.queryForList(sb.toString())).get(0)).get("REGISTRATIONID").toString();
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getTimetableRegNo :: " + stuid);
        }
        return "";
    }

    public List getWardenId(String empid) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select distinct  a.staffid from HOSTELAUTHORITIES a where a.staffid ='").append(empid).append("' and nvl(a.DEACTIVE,'N')='N'");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getWardenId :: " + empid);
        }
        return new ArrayList();
    }

    public String getAttendanceRegNo(String stuid) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT  P.REGISTRATIONID ");
        sb.append(" FROM STUDENTREGISTRATION P, REGISTRATIONMASTER Q ");
        sb.append(" WHERE P.INSTITUTEID=Q.INSTITUTEID AND P.REGISTRATIONID=Q.REGISTRATIONID ");
        sb.append(" AND Q.REGISTRATIONDATEFROM=(SELECT MAX(R.REGISTRATIONDATEFROM) FROM REGISTRATIONMASTER R ");
        sb.append(" WHERE R.REGISTRATIONID IN( SELECT DISTINCT D.REGISTRATIONID ");
        sb.append(" FROM STUDENTATTENDANCE B, FACULTYSTUDENTTAGGING C, FACULTYSUBJECTTAGGING D ");
        sb.append(" WHERE  B.INSTITUTEID=C.INSTITUTEID AND B.INSTITUTEID=D.INSTITUTEID ");
        sb.append(" AND D.FSTID=C.FSTID AND C.STUDENTFSTID=B.STUDENTFSTID ");
        sb.append(" AND B.STUDENTID='").append(stuid).append("')) AND P.STUDENTID='").append(stuid).append("' ");
        try {
            return ((LinkedCaseInsensitiveMap) (jdbcTemplate.queryForList(sb.toString())).get(0)).get("REGISTRATIONID").toString();
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getAttendanceRegNo :: " + stuid);
        }
        return "";
    }

    public String getMarksRegNo(String stuid) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT  P.REGISTRATIONID ");
        sb.append(" FROM STUDENTREGISTRATION P, REGISTRATIONMASTER Q ");
        sb.append(" WHERE P.INSTITUTEID=Q.INSTITUTEID AND P.REGISTRATIONID=Q.REGISTRATIONID ");
        sb.append(" AND Q.REGISTRATIONDATEFROM=(SELECT MAX(R.REGISTRATIONDATEFROM) FROM REGISTRATIONMASTER R ");
        sb.append(" WHERE R.REGISTRATIONID IN( SELECT DISTINCT b.REGISTRATIONID ");
        sb.append(" FROM StudentEventSubjectMarks B ");
        sb.append(" where B.STUDENTID='").append(stuid).append("')) ");
        sb.append(" AND P.STUDENTID='").append(stuid).append("' ");
        try {
            return ((LinkedCaseInsensitiveMap) (jdbcTemplate.queryForList(sb.toString())).get(0)).get("REGISTRATIONID").toString();
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getAttendanceRegNo :: " + stuid);
        }
        return "";
    }

    public List getParentDetail(String stuid) {
        StringBuilder sb = new StringBuilder();
        sb.append("select pcellno,pemailid from studentphone where studentid = '").append(stuid).append("'");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getParentDetail :: " + stuid);
        }
        return new ArrayList();
    }

    public int getHostelOutStuCancellation(Map hostelstu) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("update hosteloutgoingrequest set status ='C',approvalremarks='Cancel By Student' where requestid ='").append(hostelstu.get("requestid")).append("'");
        try {
            return jdbcTemplate.update(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getHOParentApproval :: " + hostelstu);
        }
        return i;
    }

    public int getHOParentApproval(Map hostelstu) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("update hosteloutgoingrequest set parentapprovalstatus ='").append(hostelstu.get("status")).append("',parentapprovaldatetime=TO_DATE('").append(hostelstu.get("requestdatetime")).append("', 'dd/mm/yyyy hh24:mi:ss'),parentapprovalremarks='").append(hostelstu.get("remark").toString().replaceAll("'", " ").replaceAll("\"", " ")).append("' where requestid ='").append(hostelstu.get("requestid")).append("'");
        try {
            return jdbcTemplate.update(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getHOParentApproval :: " + hostelstu);
        }
        return i;
    }

    public int getHOWardenApproval(Map hostelstu) {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("update hosteloutgoingrequest set status ='").append(hostelstu.get("status")).append("',approvalremarks='").append(hostelstu.get("remark").toString().replaceAll("'", " ").replaceAll("\"", " ")).append("',")
                .append("approvaldatetime=TO_DATE('").append(hostelstu.get("requestdatetime")).append("', 'dd/mm/yyyy hh24:mi:ss'), approvedbymemberid='").append(hostelstu.get("approvedbymemberid")).append("',")
                .append("approvedbymembertype='E'") 
                .append(" where requestid ='").append(hostelstu.get("requestid")).append("'");
        try {
            return jdbcTemplate.update(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getHOWardenApproval :: " + hostelstu);
        }
        return i;
    }

    public List getHOQrCode(Map hostelstu) {

        StringBuilder sb = new StringBuilder();
        sb.append("select requestid,requestno,status,(TO_CHAR(checkoutdatetime ,'dd/mm/yyyy hh24:mi:ss')) checkoutdate,TO_CHAR(expactedcheckindatetime ,'dd/mm/yyyy hh24:mi:ss') checkindate,nvl(parentapprovalstatus,'P') parentapprovalstatus,nvl(approvalremarks,'No remark') approvalremarks,nvl(parentapprovalremarks,'No remark') parentapprovalremarks,pleasewherevisited ");
        sb.append("from hosteloutgoingrequest   where studentid ='").append(hostelstu.get("studentid")).append("'  and requestid='").append(hostelstu.get("requestid")).append("'");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getHOWardenApproval :: " + hostelstu);
        }
        return new ArrayList();
    }
    public String getReqCount(String stuid,String da ) {

        StringBuilder sb = new StringBuilder();
        try {
        sb.append("select count(*) cc  from HOSTELOUTGOINGREQUEST  where   TO_char(CHECKOUTDATETIME,'dd/mm/yyyy' ) =  '"+sdfDate.format(sdfDate.parse(da))+"' and studentid='").append(stuid).append("' and status<>'C'");
           return ((LinkedCaseInsensitiveMap) (jdbcTemplate.queryForList(sb.toString())).get(0)).get("cc").toString();
        } catch (Exception e) {
            logger.error("MobileOracelDao :: getReqCount :: " + stuid);
        }
        return "0";
    }

    public List getStuMarks(Map studetail) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select SUBJECTCODE,EXAMEVENTCODE,WEIGHTAGE,MAXMARKS,MARKSAWARDED2,SUBJECTDESC, ");
        sb.append("((WEIGHTAGE/MAXMARKS)*MARKSAWARDED2) finalmark,MARKSENTRYPROMPT2");
        sb.append(" from V#studentmarks where STUDENTID='").append(studetail.get("studentid")).append("' and REGISTRATIONID = '")
          .append(getMarksRegNo(studetail.get("studentid").toString())).append("' order by SUBJECTCODE ");

        try {
            return jdbcTemplate.queryForList(sb.toString());

        } catch (Exception e) {
            logger.error("MobileOracelDao :: getMarksDetail :: " + studetail + " :: ");
        }
        return new ArrayList();
    }
    public List getParentStatus(String stuid) {
        StringBuilder sb = new StringBuilder();
        sb.append("select  nvl(PARENTAPPROVALSTATUS,'N') apstuts, nvl(PARENTAPPROVALREMARKS,'N') parentaremarks from HOSTELOUTGOINGREQUEST where REQUESTID =  '").append(stuid).append("'");
        try {
            return jdbcTemplate.queryForList(sb.toString());
        } catch (Exception e) {
            logger.error("Exception: MobileOracelDao :: getParentStatus :: " + stuid + " :: ");
        }
        return new ArrayList();
    }
}
